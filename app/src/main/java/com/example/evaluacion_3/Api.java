package com.example.evaluacion_3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api extends AppCompatActivity {
    ListView list;
    ArrayList <String> titles = new ArrayList<>();
    ArrayAdapter arrayAdapter = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_api);

        arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1,titles);
        list.findViewById(R.id.list);
        list.setAdapter(arrayAdapter);

        getPosts();
    }

    private void getPosts(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://farmanet.minsal.cl/index.php/ws/getLocalesTurnos")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        PostService postService = retrofit.create(PostService.class);
        Call<List<Post>> Call = postService.getPost();
        Call.enqueue(new Callback<List<Post>>() {
            @Override
            public void onResponse(retrofit2.Call<List<Post>> call, Response<List<Post>> response) {
                for (Post post : response.body()){
                titles.add(post.getLocal_nombre());
            }
            arrayAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(retrofit2.Call<List<Post>> call, Throwable t) {
                Toast.makeText(Api.this, "Error", Toast.LENGTH_LONG).show();

            }
        });
    }
}