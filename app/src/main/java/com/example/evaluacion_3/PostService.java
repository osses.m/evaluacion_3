package com.example.evaluacion_3;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;


public interface PostService {
    String API_ROUTE = "/posts";
    @GET(API_ROUTE)
    Call<List<Post> > getPost();

    class RegisterActivity extends AppCompatActivity {

        private FirebaseAuth mAuth;
        private EditText etEmail;
        private EditText etPass;
        private EditText etCpass;
        private Button btnRegister;
        private Button registerMenu;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_register);

            mAuth = FirebaseAuth.getInstance();
            etEmail = findViewById(R.id.etEmail);
            etPass = findViewById(R.id.etPass);
            etCpass = findViewById(R.id.etCpass);
            btnRegister = findViewById(R.id.btnRegister);

            registerMenu = findViewById(R.id.registerMenu); //REFERENCIA BOTON MENU

            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String email = etEmail.getText().toString();
                    String pass = etPass.getText().toString();
                    String cpass = etCpass.getText().toString();

                    if(!email.isEmpty() || !pass.isEmpty() || !cpass.isEmpty()){
                        if (pass.equals(cpass)){
                            if (pass.length()>=7){
                                createUserWithEmailAndPassword(email,pass);
                            }else {
                                Toast.makeText(RegisterActivity.this,"La clave debe tener al menos 7 caracteres", Toast.LENGTH_LONG).show();
                            }
                        }else {
                            Toast.makeText(RegisterActivity.this, "Las contraseñas ingresadas no coinciden", Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(RegisterActivity.this,"Debe llenar los campos en blanco", Toast.LENGTH_LONG).show();
                    }

                }
            });

            registerMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                    startActivity(intent);
                }
            });
        }

        @Override
        protected void onStart() {
            super.onStart();

            FirebaseUser currentUser = mAuth.getCurrentUser();
            updateUI(currentUser);
        }

        private void updateUI(FirebaseUser currentUser){
            Log.i("info", "updateUI"+ currentUser);
        }

        private void createUserWithEmailAndPassword(String email, String password){
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){
                                Log.d("TAG", "createUserWithEmail:success");
                                FirebaseUser user = mAuth.getCurrentUser();
                                Toast.makeText(RegisterActivity.this, "Usuario creado con exito", Toast.LENGTH_LONG).show();
                                cleanForm(null);
                                updateUI(user);
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w("TAG", "createUserWithEmail:failure", task.getException());
                                Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                                updateUI(null);
                            }

                        }
                    });

        }
        public void cleanForm(View view){
            etEmail.setText("");
            etPass.setText("");
            etCpass.setText("");
        }

    }
}
